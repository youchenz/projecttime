//
//  LevelMenuScene.h
//  ProjectTime
//
//  Created by Dennis Cayo on 20/5/18.
//

#ifndef LevelMenuScene_h
#define LevelMenuScene_h

#include "cocos2d.h"

class LevelMenuScene : public cocos2d::Layer
{
public:
    static LevelMenuScene* getInstance();
    // there's no 'id' in cpp, so we recommend returning the class instance pointer
    static cocos2d::Scene* createScene();
    
    // Here's a difference. Method 'init' in cocos2d-x returns bool, instead of returning 'id' in cocos2d-iphone
    virtual bool init();
    
    // implement the "static create()" method manually
    CREATE_FUNC(LevelMenuScene);
    
    CC_SYNTHESIZE(const char *, nameLevel, NameLevel);
    
private:
    void GoToGameScene( cocos2d::Ref *sender );
    
    void GoToLevelScene( cocos2d::Ref *sender,int level_name );
    
};

#endif /* LevelMenuScene_h */
