//
//  GameFinishedScene.h
//  ProjectTime
//
//  Created by Youchen Zhou on 8/6/18.
//

#ifndef GameFinishedScene_h
#define GameFinishedScene_h

#include "cocos2d.h"

class GameFinishedScene : public cocos2d::Layer
{
public:
    // there's no 'id' in cpp, so we recommend returning the class instance pointer
    static cocos2d::Scene* createScene();
    
    // Here's a difference. Method 'init' in cocos2d-x returns bool, instead of returning 'id' in cocos2d-iphone
    virtual bool init();
    
    // implement the "static create()" method manually
    CREATE_FUNC(GameFinishedScene);
    
private:
    void GoToMainMenuScene( float dt );
    
};
#endif /* GameFinishedScene_h */
