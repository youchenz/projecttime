//
//  LevelMenuScene.cpp
//  ProjectTime
//
//  Created by Dennis Cayo on 20/5/18.
//

#include <stdio.h>
#include "LevelMenuScene.h"
#include "Game.h"
#include "Definitions.h"
#include "GameScene.h"

USING_NS_CC;

Scene* LevelMenuScene::createScene()
{
    // 'scene' is an autorelease object
    auto scene = Scene::create();
    
    // 'layer' is an autorelease object
    auto layer = LevelMenuScene::create();
    
    // add layer as a child to scene
    scene->addChild(layer);
    
    // return the scene
    return scene;
}

// on "init" you need to initialize your instance
bool LevelMenuScene::init()
{
    //////////////////////////////
    // 1. super init first
    if ( !Layer::init() )
    {
        return false;
    }
    
    Size visibleSize = Director::getInstance()->getVisibleSize();
    Vec2 origin = Director::getInstance()->getVisibleOrigin();
    
    auto backgroundSprite = Sprite::create( "menu_imagenes/niveles.png" );
    backgroundSprite->setPosition( Point( visibleSize.width / 2 + origin.x, visibleSize.height / 2 + origin.y ) );
    backgroundSprite->setContentSize(Size(visibleSize.width, visibleSize.height));
    this->addChild( backgroundSprite );
    
    //Boton de Nivel 1
    auto nivel_1 = MenuItemImage::create( "menu_imagenes/nivel_1.png", "menu_imagenes/nivel_1.png", CC_CALLBACK_1( LevelMenuScene::GoToLevelScene, this, 1 ) );
    nivel_1->setPosition( Point( visibleSize.width / 6 + origin.x, visibleSize.height / 1.15 + origin.y ) );
    nivel_1->setScale(0.4f);
    
    auto menu = Menu::create( nivel_1, NULL );
    menu->setPosition( Point::ZERO );
    
    //Boton de Nivel 2
    auto nivel_2 = MenuItemImage::create( "menu_imagenes/nivel_2.png", "menu_imagenes/nivel_2.png", CC_CALLBACK_1( LevelMenuScene::GoToLevelScene, this, 2) );
    nivel_2->setPosition( Point( visibleSize.width / 1.1 + origin.x, visibleSize.height / 1.25 + origin.y ) );
    nivel_2->setScale(0.4f);
    
    auto menu2 = Menu::create( nivel_2, NULL );
    menu2->setPosition( Point::ZERO );
    
    //Boton de Nivel 3
    auto nivel_3 = MenuItemImage::create( "menu_imagenes/nivel_3.png", "menu_imagenes/nivel_3.png", CC_CALLBACK_1( LevelMenuScene::GoToLevelScene, this, 3 ) );
    nivel_3->setPosition( Point( visibleSize.width / 2.5 + origin.x, visibleSize.height / 2 + origin.y ) );
    nivel_3->setScale(0.4f);
    
    auto menu3 = Menu::create( nivel_3, NULL );
    menu3->setPosition( Point::ZERO );
    
    //Boton de Nivel 4
    auto nivel_4 = MenuItemImage::create( "menu_imagenes/nivel_4.png", "menu_imagenes/nivel_4.png", CC_CALLBACK_1( LevelMenuScene::GoToLevelScene, this, 4	 ) );
    nivel_4->setPosition( Point( visibleSize.width / 1.3 + origin.x, visibleSize.height / 6 + origin.y ) );
    nivel_4->setScale(0.4f);
    
    auto menu4 = Menu::create( nivel_4, NULL );
    menu4->setPosition( Point::ZERO );
    
    this->addChild( menu );
    this->addChild( menu2 );
    this->addChild( menu3 );
    this->addChild( menu4 );
    
    return true;
}

void LevelMenuScene::GoToGameScene( cocos2d::Ref *sender )
{
    //auto scene = GameScene::createScene();
    auto scene = Game::create();
    
    Director::getInstance( )->replaceScene( TransitionFade::create( TRANSITION_TIME, scene ) );
}

void LevelMenuScene::GoToLevelScene( cocos2d::Ref *sender, int level_name )
{
    auto userDefault = cocos2d::UserDefault::getInstance();
    userDefault->setIntegerForKey("map_name", level_name);
    
    auto scene = Game::create();
        
    Director::getInstance( )->replaceScene( TransitionFade::create( TRANSITION_TIME, scene ) );
}



