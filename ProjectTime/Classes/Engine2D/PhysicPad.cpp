//
//  PhysicPad.cpp
//  ProjectTime
//
//  Created by mastermoviles on 21/5/18.
//

#include "PhysicPad.h"

bool PhysicPad::init()
{
    VirtualControls::init();

    return true;
}

Node* PhysicPad::getNode()
{
    if(m_node==NULL)
    {
        m_node= Node::create();        
    }
    
    return m_node;
}

void PhysicPad::addTouchListeners(cocos2d::Node *node)
{
    m_listener = EventListenerController::create();
    
    
    // Registramos callbacks
    m_listener->onConnected = CC_CALLBACK_2(PhysicPad::onConnectController,this);
    m_listener->onDisconnected = CC_CALLBACK_2(PhysicPad::onDisconnectedController,this);
    m_listener->onKeyDown = CC_CALLBACK_3(PhysicPad::onKeyDown, this);
    m_listener->onKeyUp = CC_CALLBACK_3(PhysicPad::onKeyUp, this);
    m_listener->onAxisEvent = CC_CALLBACK_3(PhysicPad::onAxisEvent, this);
    
    // Añadimos el listener el mando al gestor de eventos
    node->getEventDispatcher()->addEventListenerWithSceneGraphPriority(m_listener, node);

    
    // Inicia búsqueda de controladores (necesario en iOS)
    Controller::startDiscoveryController();
}

void PhysicPad::onKeyDown(cocos2d::Controller *controller, int keyCode, cocos2d::Event *event)
{
    CCLOG("Se ha pulsado un botón del mando");
    CCLOG("KeyDown:%d", keyCode);
}

void PhysicPad::onKeyUp(cocos2d::Controller *controller, int keyCode, cocos2d::Event *event)
{
    CCLOG("Se ha soltado un botón del mando");
    CCLOG("KeyUp:%d", keyCode);
    
    if(onButtonPressed && keyCode == 1004)
    {
        onButtonPressed((Button)Button::BUTTON_ACTION);
    }
}

void PhysicPad::onAxisEvent(cocos2d::Controller* controller, int keyCode, cocos2d::Event* event)
{
    CCLOG("Notifica cambios en el stick analógico.");
    
    if (keyCode == 1000)
    {
        const auto& keyStatus = controller->getKeyStatus(keyCode);
        CCLOG("Axis KeyCode:%d Axis Value:%f", keyCode, keyStatus.value);
        
        axisState[Axis::AXIS_HORIZONTAL] = keyStatus.value;
    }
    
        /*
     Point max(m_radioStick);
     Point min(Point::ZERO-m_radioStick);
     offset.clamp(min, max);
     
     axisState[Axis::AXIS_VERTICAL] = offset.y / max.y;
     axisState[Axis::AXIS_HORIZONTAL] = offset.x / max.x;
     */
}

void PhysicPad::onConnectController(Controller* controller, Event* event)
{
    CCLOG("Game controller connected");
    CCLOG("Tag:%d", controller->getTag());
    CCLOG("Id:%d", controller->getDeviceId());
    CCLOG("Nombre:%s", controller->getDeviceName().c_str());

    physic_pad_connected = true;
}

void PhysicPad::onDisconnectedController(Controller* controller, Event* event)
{
    CCLOG("Game controller disconnected");

    physic_pad_connected = false;
}
