//
//  GameOverScene.cpp
//  ProjectTime
//
//  Created by mastermoviles on 7/6/18.
//

#include <stdio.h>

#include "GameOverScene.h"
#include "MainMenuScene.h"
#include "Definitions.h"

USING_NS_CC;

Scene* GameOverScene::createScene()
{
    // 'scene' is an autorelease object
    auto scene = Scene::create();
    
    // 'layer' is an autorelease object
    auto layer = GameOverScene::create();
    
    // add layer as a child to scene
    scene->addChild(layer);
    
    // return the scene
    return scene;
}

// on "init" you need to initialize your instance
bool GameOverScene::init()
{
    //////////////////////////////
    // 1. super init first
    if ( !Layer::init() )
    {
        return false;
    }
    
    Size visibleSize = Director::getInstance()->getVisibleSize();
    Vec2 origin = Director::getInstance()->getVisibleOrigin();
    
    this->scheduleOnce( schedule_selector( GameOverScene::GoToMainMenuScene ), DISPLAY_TIME_SPLASH_SCENE );
    
    auto backgroundSprite = Sprite::create( "menu_imagenes/fondo-game-over.png" );
    backgroundSprite->setPosition( Point( visibleSize.width / 2 + origin.x, visibleSize.height / 2 + origin.y) );
    backgroundSprite->setContentSize(Size(visibleSize.width, visibleSize.height));
    
    this->addChild( backgroundSprite );
    
    return true;
}

void GameOverScene::GoToMainMenuScene( float dt )
{
    auto scene = MainMenuScene::createScene();
    
    //Director::getInstance( )->replaceScene( TransitionFade::create( TRANSITION_TIME, scene ) );
    Director::getInstance( )->replaceScene( TransitionFade::create( TRANSITION_TIME, scene ) );
}
