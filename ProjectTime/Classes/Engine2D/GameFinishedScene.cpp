//
//  GameFinishedScene.cpp
//  ProjectTime
//
//  Created by Youchen Zhou on 8/6/18.
//

#include <stdio.h>

#include "GameFinishedScene.h"

#include "MainMenuScene.h"
#include "Definitions.h"
#include "Game.h"

USING_NS_CC;

Scene* GameFinishedScene::createScene()
{
    // 'scene' is an autorelease object
    auto scene = Scene::create();
    
    // 'layer' is an autorelease object
    auto layer = GameFinishedScene::create();
    
    // add layer as a child to scene
    scene->addChild(layer);
    
    // return the scene
    return scene;
}

// on "init" you need to initialize your instance
bool GameFinishedScene::init()
{
    //////////////////////////////
    // 1. super init first
    if ( !Layer::init() )
    {
        return false;
    }
    
    Size visibleSize = Director::getInstance()->getVisibleSize();
    Vec2 origin = Director::getInstance()->getVisibleOrigin();
    
    this->scheduleOnce( schedule_selector( GameFinishedScene::GoToMainMenuScene ), DISPLAY_TIME_SPLASH_SCENE );
    
    auto backgroundSprite = Sprite::create( "menu_imagenes/fondo-fin-nivel.png" );
    backgroundSprite->setPosition( Point( visibleSize.width / 2 + origin.x, visibleSize.height / 2 + origin.y) );
    backgroundSprite->setContentSize(Size(visibleSize.width, visibleSize.height));
    
    this->addChild( backgroundSprite );
    
    return true;
}

void GameFinishedScene::GoToMainMenuScene( float dt )
{    
    auto scene = MainMenuScene::createScene();
    
    //Director::getInstance( )->replaceScene( TransitionFade::create( TRANSITION_TIME, scene ) );
    Director::getInstance( )->replaceScene( TransitionFade::create( TRANSITION_TIME, scene ) );
}
