//
//  PhysicPad.h
//  ProjectTime
//
//  Created by mastermoviles on 21/5/18.
//

#ifndef __Mario__PhysicPad__
#define __Mario__PhysicPad__

#include "GameEntity.h"
#include "cocos2d.h"
#include "VirtualControls.h"

#define kAUTOSTICK_MARGIN   20


class PhysicPad: public VirtualControls
{
public:
    
    bool init();
    Node* getNode();
    
    virtual void addTouchListeners(cocos2d::Node *node);
    
    CREATE_FUNC(PhysicPad);
    
private:
        
    void onKeyDown(cocos2d::Controller *controller, int keyCode, cocos2d::Event *event);
    
    void onKeyUp(cocos2d::Controller *controller, int keyCode, cocos2d::Event *event);
    
    void onAxisEvent(cocos2d::Controller* controller, int keyCode, cocos2d::Event* event);
    
    void onConnectController(Controller* controller, Event* event);
    
    void onDisconnectedController(Controller* controller, Event* event);
    
    cocos2d::EventListenerController *m_listener;
    
};

#endif /* PhysicPad_h */

