#include "MainMenuScene.h"
#include "LevelMenuScene.h"
#include "Game.h"
#include "Definitions.h"


USING_NS_CC;

Scene* MainMenuScene::createScene()
{
    // 'scene' is an autorelease object
    auto scene = Scene::create();
    
    // 'layer' is an autorelease object
    auto layer = MainMenuScene::create();
    
    // add layer as a child to scene
    scene->addChild(layer);
    
    // return the scene
    return scene;
}

// on "init" you need to initialize your instance
bool MainMenuScene::init()
{
    //////////////////////////////
    // 1. super init first
    if ( !Layer::init() )
    {
        return false;
    }
    
    Size visibleSize = Director::getInstance()->getVisibleSize();
    Vec2 origin = Director::getInstance()->getVisibleOrigin();
    
    auto backgroundSprite = Sprite::create( "menu_imagenes/fondo-menu2.png" );
    backgroundSprite->setPosition( Point( visibleSize.width / 2 + origin.x, visibleSize.height / 2 + origin.y ) );
    backgroundSprite->setContentSize(Size(visibleSize.width, visibleSize.height));
    this->addChild( backgroundSprite );

    //Boton de Play
    auto playItem = MenuItemImage::create( "menu_imagenes/play.png", "menu_imagenes/play-click.png", CC_CALLBACK_1( MainMenuScene::GoToLevelScene, this ) );
    playItem->setPosition( Point( visibleSize.width / 2 + origin.x, visibleSize.height / 1.2 + origin.y ) );
    playItem->setScale(0.5f);
    
    auto menu = Menu::create( playItem, NULL );
    menu->setPosition( Point::ZERO );
    
    //Boton de Settings
    auto settingsItem = MenuItemImage::create( "menu_imagenes/menu_settings1.png", "menu_imagenes/menu_settings2.png", CC_CALLBACK_1( MainMenuScene::GoToSettingScene, this ) );
    settingsItem->setPosition( Point( visibleSize.width / 2 + origin.x, visibleSize.height / 1.5 + origin.y ) );
    settingsItem->setScale(0.37f);
    
    auto menuSettings = Menu::create( settingsItem, NULL );
    menuSettings->setPosition( Point::ZERO );
    
    //Boton de Quit
    auto quitItem = MenuItemImage::create( "menu_imagenes/quit1.png", "menu_imagenes/quit1.png", CC_CALLBACK_1( MainMenuScene::ExitGame, this ) );
    quitItem->setPosition( Point( visibleSize.width / 2 + origin.x, visibleSize.height / 2 + origin.y ) );
    quitItem->setScale(0.5f);
    
    auto menu2 = Menu::create( quitItem, NULL );
    menu2->setPosition( Point::ZERO );
    
    this->addChild( menu );
    this->addChild( menu2 );
    this->addChild( menuSettings );
    
    return true;
}

void MainMenuScene::GoToLevelScene( cocos2d::Ref *sender )
{
    auto scene = LevelMenuScene::createScene();
    //auto scene = Game::create();
    
    Director::getInstance( )->replaceScene( TransitionFade::create( TRANSITION_TIME, scene ) );
}

void MainMenuScene::GoToSettingScene( cocos2d::Ref *sender )
{
    //auto scene = GameScene::createScene();
    auto scene = Game::create();
    
    Director::getInstance( )->replaceScene( TransitionFade::create( TRANSITION_TIME, scene ) );
}

void MainMenuScene::ExitGame( cocos2d::Ref *sender )
{
    Director::getInstance( )->end();
}



