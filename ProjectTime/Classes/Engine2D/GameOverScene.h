//
//  GameOverScene.h
//  ProjectTime
//
//  Created by mastermoviles on 7/6/18.
//

#ifndef GameOverScene_h
#define GameOverScene_h

#include "cocos2d.h"

class GameOverScene : public cocos2d::Layer
{
public:
    // there's no 'id' in cpp, so we recommend returning the class instance pointer
    static cocos2d::Scene* createScene();
    
    // Here's a difference. Method 'init' in cocos2d-x returns bool, instead of returning 'id' in cocos2d-iphone
    virtual bool init();
    
    // implement the "static create()" method manually
    CREATE_FUNC(GameOverScene);
    
private:
    void GoToMainMenuScene( float dt );
    
};

#endif /* GameOverScene_h */
