//
//  GUI.cpp
//  VideojuegosCocos
//
//  Created by Fidel Aznar on 16/1/15.
//
//

#include "GUI.h"

bool GUI::init(){
    GameEntity::init();
    return true;
}

void GUI::preloadResources(){
    m_labelVidas = Label::createWithTTF("Vidas:", "fonts/Marker Felt.ttf", 24);
    m_labelMateriales = Label::createWithTTF("Materiales:", "fonts/Marker Felt.ttf", 24);
}


Node* GUI::getNode(){
    if(m_node==NULL) {
        

        Size visibleSize = Director::getInstance()->getVisibleSize();
        Vec2 origin = Director::getInstance()->getVisibleOrigin();
        
        // position the label on the center of the screen
        m_labelVidas->setPosition(Vec2(origin.x + (visibleSize.width/2)*1.5,
                                       origin.y + visibleSize.height - m_labelVidas->getContentSize().height));
        
        // position the label on the center of the screen
        m_labelMateriales->setPosition(Vec2(origin.x + (visibleSize.width/2)*0.5,
                                       origin.y + visibleSize.height - m_labelVidas->getContentSize().height));
        
        m_node= Node::create();
        // add the label as a child to this layer, orden de dibujado +1
        m_node->addChild(m_labelVidas, 1);
        m_node->addChild(m_labelMateriales, 1);
    }
    
    return m_node;
}

void GUI::setVidas(int vidas){
    m_labelVidas->setString(StringUtils::format("Vidas: %d",vidas));
}

void GUI::setMateriales(int nummateriales){
    m_labelMateriales->setString(StringUtils::format("Materiales: %d",nummateriales));
}

