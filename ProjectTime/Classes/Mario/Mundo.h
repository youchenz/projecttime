//
//  Mundo.h
//  VideojuegosCocos
//
//  Created by Fidel Aznar on 16/1/15.
//
//

#pragma once

#include "../Engine2D/GameEntity.h"
#include "../Engine2D/PhysicsGameEntity.h"
#include "../Engine2D/TiledMapHelper.h"
#include "Npc.h"
#include "CocosDebugDraw.h"


class Mundo: public PhysicsGameEntity{
    
public:
    bool init();
    ~Mundo();
    static Mundo* getInstance();
    static void destroyInstance();
    //Nombre del mapa
    const char *nombreMapa;
    
    virtual void preloadResources();
    virtual Node* getNode();
    void update(float delta);
        
    int getTileGIDAtPosition(const Point &tileCoords);
    Point tileCoordForPosition(const Vec2& position);
    Rect tileRectForPosition(const Vec2& position);
    void deleteTileAt(const Point &tileCoords);
    Size getTileSize();
    
    TiledMapHelper *getTileMapHelper() { return m_tiledMapHelper; }
    
    Vector<NPC*> getNPCs();

    // Acceso al mundo fisico
    b2World *getPhysicsWorld() { return m_world; }
    
    //Guardamos el nombre del mapa a guardar
    void setNombreMapa(const char *nombre_mapa_var);
    
    const char* nameMapaTag = "map_name";
    const int nameMApaValue = 1;

private:
    
    CREATE_FUNC(Mundo);
    static Mundo *m_instance;
    
    TiledMapHelper *m_tiledMapHelper;
    Vector<NPC*> m_npcs;

    // Mundo fisico
    b2World *m_world;
    CocosDebugDraw *m_debugDraw;

    void initPhysics();
};

