//
//  PhysicsPlayer.hpp
//  Mario
//
//  Created by Miguel Angel Lozano Ortega on 19/1/16.
//
//

#pragma once

#define DEBUG false

#define ACTION_MOVEL    001
#define ACTION_MOVER    004
#define ACTION_JUMP     002
#define ACTION_BLINK    003
#define ACTION_GRAVITY  005

#include "../Engine2D/PhysicsGameEntity.h"

class PhysicsPlayer: public PhysicsGameEntity {
public:
    
    bool init();
    
    void preloadResources();
    Node* getNode();
    
    const Vec2& getPosition(){
        return m_playerSprite->getPosition();
    };
    
    void setPosition(const Vec2& pos) {
        this->setTransform(pos, 0.0f);
    }
    
    virtual void update(float dt);
    
    int getNumVidas(){return m_vidas; };
    int getNumMateriales(){return m_materiales; };
    
    void move(float horizontalAxis, float dt);
    void run(float horizontalAxis, float dt);
    void jump();
    void die();
    bool checkGrounded();
    void obtenidoMaterial();
    
    CREATE_FUNC(PhysicsPlayer);
    
private:
    Sprite *m_playerSprite;
    b2Fixture *m_groundTest;
    Speed *m_actionAndar;
    Animation *m_animAndar;

    b2Fixture *m_fixture;
    
    int m_vidas=3;
    int m_materiales=0;
    float m_horizontalAxis;
    bool m_grounded;
    const float m_vel=80;
    const float m_jump=6.0f;
    
};


