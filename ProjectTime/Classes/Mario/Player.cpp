//
//  Player.cpp
//  VideojuegosCocos
//
//  Created by Fidel Aznar on 14/1/15.
//
//

// Posible ejercicio: que no tenga doble salto

#include "Player.h"
#include "Mundo.h"


bool Player::init(){
    GameEntity::init();
    m_playerSprite = Sprite::create();
    m_state = PlayerState::Stopped;
    return true;
}

void Player::preloadResources(){
    
    //Cache de sprites
    auto spriteFrameCache = SpriteFrameCache::getInstance();
    //Cache de animaciones
    auto animCache = AnimationCache::getInstance();
    
    //Si no estaba el spritesheet en la caché lo cargo
    if(!spriteFrameCache->getSpriteFrameByName("run-1.png")) {
        spriteFrameCache->addSpriteFramesWithFile("sprites.plist");
    }
    
    m_animAndar = Animation::create();
    m_animAndar->addSpriteFrame(spriteFrameCache->getSpriteFrameByName("run-1.png"));
    m_animAndar->addSpriteFrame(spriteFrameCache->getSpriteFrameByName("run-2.png"));
    m_animAndar->addSpriteFrame(spriteFrameCache->getSpriteFrameByName("run-3.png"));
    m_animAndar->addSpriteFrame(spriteFrameCache->getSpriteFrameByName("run-4.png"));
    m_animAndar->addSpriteFrame(spriteFrameCache->getSpriteFrameByName("run-5.png"));
    m_animAndar->addSpriteFrame(spriteFrameCache->getSpriteFrameByName("run-6.png"));
    m_animAndar->addSpriteFrame(spriteFrameCache->getSpriteFrameByName("run-7.png"));
    m_animAndar->addSpriteFrame(spriteFrameCache->getSpriteFrameByName("run-8.png"));
    m_animAndar->setDelayPerUnit(0.1);
    
    animCache->addAnimation(m_animAndar, "animAndar");
    
}

Node* Player::getNode(){
    
    if(m_node==NULL){
        Size visibleSize = Director::getInstance()->getVisibleSize();
        Vec2 origin = Director::getInstance()->getVisibleOrigin();
        
        m_playerSprite = Sprite::createWithSpriteFrameName("run-1.png");
        m_playerSprite->setAnchorPoint(Vec2(0.5, 0));
        m_playerSprite->setPosition(visibleSize.width/2 + origin.x, 0 + origin.y);
        
        m_node = m_playerSprite;
        m_node->setLocalZOrder(1);
    }
    
    return m_node;
    
    
}

void Player::move(float axis, float dt) {
    if(axis > 0.5f) {
        this->setPosition(this->getPosition() + Vec2::UNIT_X * m_vel * dt);
        walk(true);
    } else if(axis < -0.5f) {
        this->setPosition(this->getPosition() - Vec2::UNIT_X * m_vel * dt);
        walk(false);
    } else {
        this->stop();
    }
}

void Player::walk(bool right) {
    if(m_state == PlayerState::Stopped) {
        Animate *actionAnimate = Animate::create(AnimationCache::getInstance()->getAnimation("animAndar"));
        Action* action = RepeatForever::create(actionAnimate);
        action->setTag(ACTION_ANIM);
        m_playerSprite->runAction(action);
    }
    
    if(right) {
        m_playerSprite->setFlippedX(false);
        m_state = PlayerState::WalkingRight;
    } else {
        m_playerSprite->setFlippedX(true);
        m_state = PlayerState::WalkingLeft;
    }
}

void Player::stop() {
    if(m_state != PlayerState::Stopped) {
        m_playerSprite->stopActionByTag(ACTION_ANIM);
        m_playerSprite->setSpriteFrame("run-1.png");
        m_state = PlayerState::Stopped;
    }
}

void Player::jump(){
    auto actionAnimate = Animate::create(AnimationCache::getInstance()->getAnimation("animAndar"));
    ActionInterval*jump;
    jump = JumpBy::create(1,Point(0,0),m_jump,1);
    
    auto combine1 = Spawn::create(actionAnimate,jump, NULL);
    combine1->setTag(ACTION_JUMP);
    
    m_playerSprite->runAction(combine1);
}

void Player::die(){
    
    //Si no se esta parpadenado, parpadeo y quito vida
    if(!m_playerSprite->getActionByTag(ACTION_BLINK)){
        m_vidas--;
        
        auto action = Sequence::create(Blink::create(3,30),Show::create(),NULL);
        action->setTag(ACTION_BLINK);
        m_playerSprite->runAction(action);
    }
}

void Player::testWorldCollisions(Mundo *mundo){
    
    auto pos = getPosition();
    auto player = getNode();
    
    int tilemargin = mundo->getTileSize().width/2.0;
    
    //Colisiones de Player con el mundo (izq,der)
    //Derecha
    if(mundo->getTileGIDAtPosition(Vec2(pos.x+tilemargin,pos.y))!=0) {
        Rect tileRect = mundo->tileRectForPosition(Vec2(pos.x+tilemargin,pos.y));
        player->setPositionX(tileRect.origin.x - player->getContentSize().width/2);
        stop();
    }
    
    //Izquierda
    if(mundo->getTileGIDAtPosition(Vec2(pos.x-tilemargin,pos.y))!=0) {
        Rect tileRect = mundo->tileRectForPosition(Vec2(pos.x-tilemargin,pos.y));
        player->setPositionX(tileRect.origin.x + tileRect.size.width + player->getContentSize().width/2);
        stop();
    }
    
    //Colision arriba (romper bloques)
    if(mundo->getTileGIDAtPosition(Vec2(pos.x,pos.y+tilemargin))==3) {
        mundo->deleteTileAt(mundo->tileCoordForPosition(Vec2(pos.x,pos.y+9)));
    }
    
}

