//
//  Mundo.cpp
//  VideojuegosCocos
//
//  Created by Fidel Aznar on 16/1/15.
//
//

#include "Mundo.h"
#include "iostream"


bool Mundo::init(){
    GameEntity::init();
    
    m_tiledMapHelper = TiledMapHelper::create();
    m_tiledMapHelper->retain();
    
    // Inicialización del mundo físico
    m_world = new b2World(b2Vec2(0,-10));
    
    m_debugDraw = new CocosDebugDraw(PTM_RATIO);
    m_world->SetDebugDraw(m_debugDraw);
    
    uint32 flags = 0;
    flags += b2Draw::e_shapeBit;
    //flags += b2Draw::e_jointBit;
    //flags += b2Draw::e_aabbBit;
    //flags += b2Draw::e_pairBit;
    //flags += b2Draw::e_centerOfMassBit;
    
    m_debugDraw->SetFlags(flags);
    
    return true;
}

void Mundo::destroyInstance() {
    if(m_instance!=NULL) {
        m_instance->release();
        m_instance=NULL;
    }
}

Mundo* Mundo::getInstance(){
    if(Mundo::m_instance==NULL) {
        Mundo::m_instance = Mundo::create();
        //Hago un retain hasta el final de los tiempos, soy un singleton
        m_instance->retain();
    }
    
    return Mundo::m_instance;
}

Mundo::~Mundo(){
    CCLOG("Mundo es Singleton, no debería morir hasta el final. Cuidado...");
    m_tiledMapHelper->release();
    if(m_instance!=NULL) {
        m_instance->release();
        m_instance=NULL;
    }
    
    if(m_world != NULL) {
        delete m_world;
        m_world = NULL;
    }
    if(m_debugDraw != NULL) {
        delete m_debugDraw;
        m_debugDraw = NULL;
    }
    
    for (auto npc: this->m_npcs)
    {
        npc->release();
    }
}

void Mundo::preloadResources(){
    
    //TODO: Set nombre del mapa a cargar
    auto userDefault = cocos2d::UserDefault::getInstance();
    int nameMapValueGlobal = userDefault->getIntegerForKey(nameMapaTag);
    switch (nameMapValueGlobal) {
        case 1:     this->setNombreMapa("jungle-nivel_1.tmx");
            break;
        case 2:     this->setNombreMapa("jungle-nivel_2.tmx");
            break;
        case 3:     this->setNombreMapa("jungle-nivel_3.tmx");
            break;
        case 4:     this->setNombreMapa("jungle-nivel_4.tmx");
            break;
        default:    this->setNombreMapa("jungle-nivel_2.tmx");
            break;
    }
    
    
    m_tiledMapHelper->loadTileMap(this->nombreMapa, "Foreground", "Colectables", "Comida", "Final");
    //m_tiledMapHelper->loadTileMap("jungle-nivel2.tmx", "Foreground", "Colectables", "Comida", "Final");
    
    m_npcs.clear();
    
    //Creo los NPCs
    m_npcs.pushBack(NPC::create());
    m_npcs.pushBack(NPC::create());
    
    //Precargo recursos de los NPC
    int k=2;
    for (auto npc: this->m_npcs){
        npc->preloadResources();
        if(k==0){
            npc->initPos.x = 100;
            npc->initPos.y = 0;
        }
        if(k==1){
            npc->initPos.x = 900;
            npc->initPos.y = 0;
        }
        
        k++;
    }
    
    // (b) Descomentar. Inicializa físicas.
    initPhysics();
    
}

void Mundo::initPhysics() {
    b2BodyDef bodyDef;
    bodyDef.position.Set(0, 0);
    bodyDef.type = b2BodyType::b2_staticBody;
    
    m_body = m_world->CreateBody(&bodyDef);
    
    b2FixtureDef fixtureDef;
    
    TMXObjectGroup *groupPhysics = m_tiledMapHelper->getTiledMap()->getObjectGroup("Fisica");
    
    auto objects = groupPhysics->getObjects();
    
    for(auto object : objects) {
        ValueVector polyline = object.asValueMap().at("polylinePoints").asValueVector();
        float x = object.asValueMap().at("x").asFloat() + m_tiledMapHelper->getTiledMap()->getPositionX();
        float y = object.asValueMap().at("y").asFloat() + m_tiledMapHelper->getTiledMap()->getPositionY();
        
        //Genera cadena en box2d
        int numPoints = (int)polyline.size();
        b2Vec2 *chainPoints = new b2Vec2[numPoints];
        
        int i=0;
        for(auto point: polyline) {
            chainPoints[i++].Set((x+point.asValueMap().at("x").asFloat()) / PTM_RATIO,
                                 (y-point.asValueMap().at("y").asFloat()) / PTM_RATIO);
        }
        
        b2ChainShape chain;
        chain.CreateChain(chainPoints, numPoints);
        fixtureDef.shape = &chain;
        
        m_body->CreateFixture(&fixtureDef);
    }
}

Node* Mundo::getNode(){
    
    Node *node = Node::create();

    //Añado los NPCS en las posiciones que corresponda
    for (auto npc: this->m_npcs){
        Node *n = npc->getNode();
        node->addChild(n);
    }
    
    //Añado el tiledmap
    node->addChild(m_tiledMapHelper->getTiledMap());
    
    //Añado nodo debug de fisicas
    node->addChild(m_debugDraw->GetNode(),9999);
    
    return node;
    
}

int Mundo::getTileGIDAtPosition(const Point &tileCoords){
    return m_tiledMapHelper->getTileGIDAtPosition(tileCoords);
}

Point Mundo::tileCoordForPosition(const Vec2& position){
   return m_tiledMapHelper->tileCoordForPosition(position);
}

Rect Mundo::tileRectForPosition(const cocos2d::Vec2 &position) {
    Point tileCoords = m_tiledMapHelper->tileCoordForPosition(position);
    return m_tiledMapHelper->rectForTileAt(tileCoords);
}

void Mundo::deleteTileAt(const Point &tileCoords){
    auto sprite = m_tiledMapHelper->getCollisionLayer()->getTileAt(tileCoords);
    
    //Labda para borrar el tile cuando termine la animación
    //& copia por referencia
    //= copia por valor
    //Cuidado, pq tileCoords estara liberado cuando se ejecute lambda
    //Por defecto [=]
    auto actionDelete = CallFunc::create([&,tileCoords](){m_tiledMapHelper->deleteTileAt(tileCoords);});
    
    auto actionAnim = Spawn::create(MoveBy::create(.1, Vec2(0,5)),FadeOut::create(1),RotateBy::create(1,90),NULL);
    auto action = Sequence::create(actionAnim,actionDelete,NULL);
    
    sprite->runAction(action);

}

Size Mundo::getTileSize(){
    return m_tiledMapHelper->getTiledMap()->getTileSize();
}

Vector<NPC*> Mundo::getNPCs(){
    return m_npcs;
}

void Mundo::update(float delta){
    // Actualiza físicas
    m_debugDraw->Clear();
    m_world->DrawDebugData();
    
    m_world->ClearForces();
    m_world->Step(1.0f/60.0f, 6, 2);
    
    for (auto npc: this->m_npcs){
        npc->update(delta);
    }
}

void Mundo::setNombreMapa( const char *nombre_mapa_var)
{
    this->nombreMapa = nombre_mapa_var;
}

