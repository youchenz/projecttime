//
//  Player.h
//  VideojuegosCocos
//
//  Created by Fidel Aznar on 14/1/15.
//
//

#pragma once

#define DEBUG false

#define ACTION_ANIM     001
#define ACTION_JUMP     002
#define ACTION_BLINK    003

#include "../Engine2D/GameEntity.h"

class Mundo;

enum PlayerState {
    WalkingLeft,
    WalkingRight,
    Stopped
};

class Player: public GameEntity{
public:
    
    bool init();
    
    void preloadResources();
    Node* getNode();
    
    const Vec2& getPosition(){
        return m_playerSprite->getPosition();
    };
    
    void setPosition(const Vec2& pos) {
        m_playerSprite->setPosition(pos);
    }
    
    int getNumVidas(){ return m_vidas; };
    
    void move(float axis, float dt);
    void walk(bool right);
    void stop();
    void jump();
    void die();
    
    void testWorldCollisions(Mundo*);
    
    
    CREATE_FUNC(Player);
    
private:
    Sprite *m_playerSprite;
    Animation *m_animAndar;
    PlayerState m_state;
    int m_vidas=3;
    const int m_vel=80;
    const int m_jump=40;
    
};


