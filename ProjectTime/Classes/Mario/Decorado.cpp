//
//  Decorado.cpp
//  VideojuegosCocos
//
//  Created by Fidel Aznar on 16/1/15.
//
//

#include "Decorado.h"

bool Decorado::init(){
    GameEntity::init();
    return true;
}



Node* Decorado::getNode(){
    if(m_node==NULL) {
        
        Size visibleSize = Director::getInstance()->getVisibleSize();
        
       
        Sprite *sprite = Sprite::create("plx-1-mapa.png");
        Sprite *sprite1 = Sprite::create("plx-2-mapa.png");
        Sprite *sprite2 = Sprite::create("plx-3-mapa.png");
        Sprite *sprite3 = Sprite::create("plx-4-mapa.png");
        Sprite *sprite4 = Sprite::create("plx-5-mapa.png");
        
//        TMXTiledMap *prueba = TMXTiledMap::create("plx-1-mapa.tmx");
        
//        prueba->setContentSize(Size(visibleSize.width, visibleSize.height));
        sprite->setContentSize(Size(visibleSize.width*4, visibleSize.height));
        sprite1->setContentSize(Size(visibleSize.width*4, visibleSize.height));
        sprite2->setContentSize(Size(visibleSize.width*4, visibleSize.height));
        sprite3->setContentSize(Size(visibleSize.width*4, visibleSize.height));
        sprite4->setContentSize(Size(visibleSize.width*4, visibleSize.height));
        
        ParallaxNode *parallaxNode = ParallaxNode::create();
//        parallaxNode->addChild(prueba, 1, Vec2(0.4, 1), Vec2(250, 160));
        parallaxNode->addChild(sprite, 1, Vec2(0.4, 1), Vec2(250,128));
        parallaxNode->addChild(sprite1, 2, Vec2(0.45, 1), Vec2(250,128));
        parallaxNode->addChild(sprite2, 3, Vec2(0.5, 1), Vec2(250,128));
        parallaxNode->addChild(sprite3, 4, Vec2(0.55, 1), Vec2(250,128));
        parallaxNode->addChild(sprite4, 5, Vec2(0.6, 1), Vec2(250,128));
        
        m_node= Node::create();
        m_node->addChild(parallaxNode,0);
        m_node->setLocalZOrder(-1);
    }
    
    return m_node;
}


