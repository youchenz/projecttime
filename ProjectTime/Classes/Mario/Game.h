//
//  MarioSc.h
//  Test01
//
//  Created by Fidel Aznar on 13/1/15.
//
//

#pragma once

#include "../Engine2D/SimpleGame.h"
#include "Player.h"
#include "PhysicsPlayer.h"
#include "VirtualControls.h"
#include "Npc.h"
#include "Decorado.h"
#include "GUI.h"
#include "Mundo.h"
#include <time.h>

class Game: public SimpleGame{
    
public:
    
    bool init();
        ~Game();
    
    void preloadResources();
    void start();
    
    void updateEachFrame(float delta);
    
    void testNPCCollisionsWithPlayer();
    
    void testColectablesCollisionsWithPlayer();
    
    void testComidaCollisionsWithPlayer();
    
    void testFinalCollisionsWithPlayer();
    
    Point tileCoordForPosition(Point position);
    
    // (d) Comentar o eliminar los eventos de teclado
    
//    void onKeyPressed(EventKeyboard::KeyCode keyCode, cocos2d::Event *event);
//    void onKeyReleased(EventKeyboard::KeyCode keyCode, cocos2d::Event *event);
    
    // (d) Descomentar. Controles virtuales
    
    void onButtonPressed(Button button);
    void onButtonReleased(Button button);
    
    void mostrarGameOver();
    void mostrarFinNivel();

    void calcularFinRun();

    void moverPersonaje(float delta);
    
    // implement the "static create()" method manually
    CREATE_FUNC(Game);    

    
private:
    //contadores de logros
    int m_coleccionable_count;
    int m_comida_count;
    
    
    // (c) Descomenta PhysicsPlayer y comenta Player
    PhysicsPlayer *m_player;
//    Player *m_player;
    
    Decorado *m_decorado;
    GUI *m_gui;
    Mundo *m_mundo;
    VirtualControls *m_input_physic;
    VirtualControls *m_input_virtual;
    TiledMapHelper *m_tiledMapHelper;

    float m_horizontalAxis;
    
    Node *m_scrollable;

    time_t tiempo_inicio_run;
    bool run;
};


