//
//  PhysicsPlayer.cpp
//  ProjectTime
//
//  Created by Miguel Angel Lozano Ortega on 19/1/16.
//
//

#include "PhysicsPlayer.h"


#include "Mundo.h"


bool PhysicsPlayer::init(){
    GameEntity::init();
    m_playerSprite = Sprite::create();
    return true;
}

void PhysicsPlayer::preloadResources(){
    
    //Cache de sprites
    auto spriteFrameCache = SpriteFrameCache::getInstance();
    //Cache de animaciones
    auto animCache = AnimationCache::getInstance();
    
    //Si no estaba el spritesheet en la caché lo cargo
    if(!spriteFrameCache->getSpriteFrameByName("run-1.png")) {
        spriteFrameCache->addSpriteFramesWithFile("run.plist");
    }

    m_body = NULL;
    m_groundTest = NULL;
    
    Animation *animAndar = Animation::create();
    animAndar->addSpriteFrame(spriteFrameCache->getSpriteFrameByName("run-1.png"));
    animAndar->addSpriteFrame(spriteFrameCache->getSpriteFrameByName("run-2.png"));
    animAndar->addSpriteFrame(spriteFrameCache->getSpriteFrameByName("run-3.png"));
    animAndar->addSpriteFrame(spriteFrameCache->getSpriteFrameByName("run-4.png"));
    animAndar->addSpriteFrame(spriteFrameCache->getSpriteFrameByName("run-5.png"));
    animAndar->addSpriteFrame(spriteFrameCache->getSpriteFrameByName("run-6.png"));
    animAndar->addSpriteFrame(spriteFrameCache->getSpriteFrameByName("run-7.png"));
    animAndar->addSpriteFrame(spriteFrameCache->getSpriteFrameByName("run-8.png"));
    animAndar->setDelayPerUnit(0.1);
    
    animCache->addAnimation(animAndar, "animAndar");
    
    m_animAndar = animAndar;
}

Node* PhysicsPlayer::getNode(){
    
    if(m_node==NULL){
        Size visibleSize = Director::getInstance()->getVisibleSize();
        Vec2 origin = Director::getInstance()->getVisibleOrigin();
        
        m_playerSprite = Sprite::createWithSpriteFrameName("run-1.png");
        
        m_node = m_playerSprite;
        m_node->setLocalZOrder(1);
        // Crea cuerpo físico
        b2World *world = Mundo::getInstance()->getPhysicsWorld();
        
        b2BodyDef bodyDef;
        bodyDef.type = b2BodyType::b2_dynamicBody;
        bodyDef.fixedRotation = true;

        b2PolygonShape shapeBoundingBox;
        shapeBoundingBox.SetAsBox(m_playerSprite->getContentSize().width * 0.5 / PTM_RATIO,
                                  m_playerSprite->getContentSize().height * 0.5 / PTM_RATIO);
        
        m_body = world->CreateBody(&bodyDef);
        m_fixture = m_body->CreateFixture(&shapeBoundingBox, 1.0);
        m_fixture->SetFriction(0.0f);
        
        b2PolygonShape shapeSensor;
        shapeSensor.SetAsBox(m_playerSprite->getContentSize().width * 0.35 / PTM_RATIO, 2.0 / PTM_RATIO, b2Vec2(0, -m_playerSprite->getContentSize().height * 0.5 / PTM_RATIO), 0.0f);
        
        m_groundTest = m_body->CreateFixture(&shapeSensor, 1.0);
        m_groundTest->SetSensor(true);

        this->setTransform(Vec2(visibleSize.width/2 + origin.x+20, visibleSize.height/2 + origin.y+30), 0.0f);
        
        Animate* actionAnimate = Animate::create(AnimationCache::getInstance()->getAnimation("animAndar"));
        RepeatForever* actionRepeat = RepeatForever::create(actionAnimate);
        Speed* actionSpeed = Speed::create(actionRepeat, 1.0f);
        
        m_playerSprite->runAction(actionSpeed);
        m_actionAndar = actionSpeed;
    }
    
    return m_node;
    
    
}

bool PhysicsPlayer::checkGrounded() {
    
    b2ContactEdge *edge = m_body->GetContactList();
    while ( edge != NULL ) {
        if ( edge->contact->GetFixtureA()==m_groundTest || edge->contact->GetFixtureB()==m_groundTest) {
            if(edge->contact->IsTouching()) {
                return true;
            }
        }
        edge = edge->next;
    }
    return false;
}

void PhysicsPlayer::update(float delta) {
    PhysicsGameEntity::update(delta);
    
    if(fabsf(m_horizontalAxis) < 0.1) {
        m_actionAndar->setSpeed(0.0f);
        m_playerSprite->setSpriteFrame("run-1.png");
    } else {
        m_actionAndar->setSpeed(fabsf(m_horizontalAxis));
    }
    
    m_body->SetLinearVelocity(b2Vec2(m_horizontalAxis * m_vel / PTM_RATIO, m_body->GetLinearVelocity().y));
    
}

void PhysicsPlayer::move(float horizontalAxis, float delta) {
    if(horizontalAxis < 0 && !m_playerSprite->isFlippedX()) {
        m_playerSprite->setFlippedX(true);
    } else if(horizontalAxis > 0 && m_playerSprite->isFlippedX()) {
        m_playerSprite->setFlippedX(false);
    }

    m_horizontalAxis = clampf(horizontalAxis, -1.0f, 1.0f);
}

void PhysicsPlayer::run(float horizontalAxis, float delta) {
    if(horizontalAxis < 0 && !m_playerSprite->isFlippedX()) {
        m_playerSprite->setFlippedX(true);
    } else if(horizontalAxis > 0 && m_playerSprite->isFlippedX()) {
        m_playerSprite->setFlippedX(false);
    }

    m_horizontalAxis = clampf(horizontalAxis * 2, -2.0f, 2.0f);
}

void PhysicsPlayer::jump() {
    if(checkGrounded()) {
        m_body->SetLinearVelocity(b2Vec2(m_body->GetLinearVelocity().x, m_jump));
    }
}

void PhysicsPlayer::die(){
    
    //Si no se esta parpadenado, parpadeo y quito vida
    if(!m_playerSprite->getActionByTag(ACTION_BLINK)){
        m_vidas--;
        auto action = Sequence::create(Blink::create(3,30),Show::create(),NULL);
        action->setTag(ACTION_BLINK);
        m_playerSprite->runAction(action);
    }

    if (m_vidas <= 0)
    {
        m_body->SetActive(false);
    }
}

void PhysicsPlayer::obtenidoMaterial() {
    m_materiales++;
}
