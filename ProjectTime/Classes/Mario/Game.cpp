//
//  MarioSc.cpp
//  Test01
//
//  Created by Fidel Aznar on 13/1/15.
//
//

#include "Game.h"
#include "VirtualPad.h"
#include "VirtualStick.h"
#include "VirtualStickAuto.h"
#include "PhysicPad.h"
#include "GameOverScene.h"
#include "GameFinishedScene.h"
#include "Definitions.h"

#if SDKBOX_ENABLED
#include "PluginSdkboxPlay/PluginSdkboxPlay.h"
#endif

Mundo* Mundo::m_instance = NULL;

bool Game::init(){
    
    
    
    SimpleGame::init();
    
    m_decorado = Decorado::create();
    m_decorado->retain();
    addGameEntity(m_decorado);
    
    
    m_mundo = Mundo::getInstance();
    
    addGameEntity(m_mundo);
    
    m_tiledMapHelper = m_mundo->getTileMapHelper();
    
    m_gui = GUI::create();
    m_gui->retain();
    addGameEntity(m_gui);
    
    // (c) Haz que la incialización de m_player sea como PhysicsPlayer en lugar de Player
    m_player = PhysicsPlayer::create();
//    m_player = Player::create();
    m_player->retain();
    addGameEntity(m_player);
    
    // (d) Descomentar. Controles virtuales
    
    // Controles virtuales
    m_input_virtual = VirtualStickAuto::create();
    m_input_virtual->retain();
    addGameEntity(m_input_virtual);
    
    m_input_physic = PhysicPad::create();
    m_input_physic->retain();
    addGameEntity(m_input_physic);

    
    CCLOG("Entra en GAME.CPP");

    m_scrollable = Node::create();
    
    m_horizontalAxis = 0.0f;

    run = false;
    
    this->preloadResources();
    this->start();
    
    return true;
}

Game::~Game(){
    m_player->release();
    m_decorado->release();
    m_gui->release();

    // (d) Descomentar. Liberación de memoria de controles virtuales
    m_input_physic->release();
    m_input_virtual->release();
}

void Game::preloadResources(){
    preloadEachGameEntity();
}

void Game::start(){
    
    addEachGameEntityNodeTo(m_scrollable);
    m_scrollable->removeChild(m_gui->getNode());
    // (d) Descomentar. Eliminar controles virtuales del scroll
    m_scrollable->removeChild(m_input_physic->getNode());
    m_scrollable->removeChild(m_input_virtual->getNode());
    
    addChild(m_scrollable);
    addChild(m_gui->getNode());

    // (d) Descomentar. Añadir controles virtuales sin scroll
    addChild(m_input_physic->getNode());
    addChild(m_input_virtual->getNode());

    // (d) Descomentar. Registro de eventos de controles virtuales
    
    m_input_physic->onButtonPressed = CC_CALLBACK_1(Game::onButtonPressed, this);
    m_input_physic->onButtonReleased = CC_CALLBACK_1(Game::onButtonReleased, this);
    
    m_input_virtual->onButtonPressed = CC_CALLBACK_1(Game::onButtonPressed, this);
    m_input_virtual->onButtonReleased = CC_CALLBACK_1(Game::onButtonReleased, this);
    
    m_input_physic->addTouchListeners(this);
    m_input_physic->addKeyboardListeners(this);
    
    m_input_virtual->addTouchListeners(this);
    m_input_virtual->addKeyboardListeners(this);
    
    m_gui->setVidas(m_player->getNumVidas());
    m_gui->setMateriales(m_player->getNumMateriales());
    
    Vec2 position_player = m_player->getPosition();
    Size visibleSize = Director::getInstance()->getVisibleSize();
    
    // El 62 es porque el personaje es 16x31, entonces es el doble de alto. Cogemos 2 por y
    m_scrollable->setPosition(Vec2(visibleSize.width / 2 - position_player.x, 62));
    
    //contadores de logros
     m_coleccionable_count = 0;
     m_comida_count = 0;
}

void Game::moverPersonaje(float delta)
{
    if (m_input_physic->physic_pad_connected == true || m_input_virtual->physic_pad_connected == true)
    {
        if (run == true)
        {
            m_player->run(m_input_physic->getAxis(Axis::AXIS_HORIZONTAL), delta);
            calcularFinRun();
        }
        else
        {
            m_player->move(m_input_physic->getAxis(Axis::AXIS_HORIZONTAL), delta);
        }
    }
    else //if (m_input_virtual->getAxis(Axis::AXIS_HORIZONTAL) != 0)
    {
        if (run == true)
        {
            m_player->run(m_input_virtual->getAxis(Axis::AXIS_HORIZONTAL), delta);
            calcularFinRun();
        }
        else
        {
            m_player->move(m_input_virtual->getAxis(Axis::AXIS_HORIZONTAL), delta);
        }
    }
}

void Game::updateEachFrame(float delta){

    updateEachGameEntityWithDelta(delta);

    // (d) Descomentar. Control de usuario con eje horizontal de controles virtuales
    moverPersonaje(delta);
    
    
    Vec2 position_player = m_player->getPosition();
    Size visibleSize = Director::getInstance()->getVisibleSize();
    
    m_scrollable->setPosition(Vec2(visibleSize.width / 2 - position_player.x, 62));
    
    //Lo pongo aqui para evitar que la interpolación de las acciones en updateIA haga pequeños saltos al subir el personaje
    //el consumo de recursos es minimo. Solo para player
    
    testNPCCollisionsWithPlayer();

    testColectablesCollisionsWithPlayer();
    
    testComidaCollisionsWithPlayer();
    
    testFinalCollisionsWithPlayer();
    
    // (c) Comenta o elimina la siguiente linea al pasar a usar fisicas (ya no hace falta)
//    m_player->testWorldCollisions(m_mundo);

}

void Game::testColectablesCollisionsWithPlayer() {
    
    Point tileCoords = this->tileCoordForPosition(m_player->getNode()->getPosition());
    TMXLayer *colectables = m_tiledMapHelper->getColectableLayer();
    
    int tileGid = colectables->getTileGIDAt(tileCoords);

    if (tileGid) {
        m_coleccionable_count++;
        #if SDKBOX_ENABLED
            if(m_coleccionable_count==3){
                sdkbox::PluginSdkboxPlay::unlockAchievement("Confía en la suerte");
            }
        #endif
        
        Point auxCoords;
        for (int i=-3; i<=3; i++) {
            for (int j=-3; j<=3; j++) {
                auxCoords.x = tileCoords.x + i;
                auxCoords.y = tileCoords.y + j;
                if (auxCoords.x > 0 && auxCoords.y > 0) {
                colectables->removeTileAt(auxCoords);
                }
            }
        }
        
        m_player->obtenidoMaterial();
        m_gui->setMateriales(m_player->getNumMateriales());
    }
    
}

void Game::testComidaCollisionsWithPlayer() {
    
    Point tileCoords = this->tileCoordForPosition(m_player->getNode()->getPosition());
    TMXLayer *comida = m_tiledMapHelper->getComidaLayer();
    
    int tileGid = comida->getTileGIDAt(tileCoords);
    
    if (tileGid) {
        m_comida_count++;
        #if SDKBOX_ENABLED
            if(m_comida_count==1){
                sdkbox::PluginSdkboxPlay::unlockAchievement("Bon profit");
            }
        #endif
        
        Point auxCoords;
        for (int i=-3; i<=3; i++) {
            for (int j=-3; j<=3; j++) {
                auxCoords.x = tileCoords.x + i;
                auxCoords.y = tileCoords.y + j;
                if (auxCoords.x > 0 && auxCoords.y > 0) {
                    comida->removeTileAt(auxCoords);
                }
            }
        }
        run = true;
        tiempo_inicio_run = time (NULL);
    }
}

void Game::testFinalCollisionsWithPlayer() {
    Point tileCoords = this->tileCoordForPosition(m_player->getNode()->getPosition());
    TMXLayer *final = m_tiledMapHelper->getFinalLayer();
    
    int tileGid = final->getTileGIDAt(tileCoords);
    
    if (tileGid) {
        #if SDKBOX_ENABLED
            sdkbox::PluginSdkboxPlay::unlockAchievement("¿Qué tiempo es este?");
        #endif
        mostrarFinNivel();
    }
}


Point Game::tileCoordForPosition(Point position)
{
    TMXTiledMap *_tiledMap = m_tiledMapHelper->getTiledMap();
    Size tileSize = _tiledMap->getTileSize();
    
    float totalHeight = _tiledMap->getMapSize().height * tileSize.height;
    float x = floor(position.x / tileSize.width);
    float y = floor((totalHeight - position.y) / tileSize.height) - 3;
    if(y < 0)
        y = 0;
    return Point(x, y);
}

void Game::mostrarGameOver()
{
    auto director = Director::getInstance();
    
    auto scene = GameOverScene::createScene();
    
    director->replaceScene( TransitionFade::create( TRANSITION_TIME, scene ) );
    
}

void Game::mostrarFinNivel() {
    
    auto director = Director::getInstance();
    
    auto scene = GameFinishedScene::createScene();
    
    director->replaceScene( TransitionFade::create( TRANSITION_TIME, scene ) );
}

void Game::testNPCCollisionsWithPlayer(){
    
    //Compruebo colisiones entre Player y el primer NPC...
    auto bplayer = m_player->getNode()->getBoundingBox();
    auto bnpc = m_mundo->getNPCs().front()->getNode()->getBoundingBox();
    
    if (bplayer.intersectsRect(bnpc)) {
        m_player->die();
        m_gui->setVidas(m_player->getNumVidas());
    }
    
    if (m_player->getNumVidas() <= 0)
    {
        mostrarGameOver();
    }
}

// (d) Comentar o eliminar onKeyPressed y onKeyReleased (Control de teclado)
/*
void Game::onKeyPressed(EventKeyboard::KeyCode keyCode, cocos2d::Event *event){
    
    if (keyCode == EventKeyboard::KeyCode::KEY_LEFT_ARROW)
    {
        m_horizontalAxis -= 1.f;
    }
    if (keyCode == EventKeyboard::KeyCode::KEY_RIGHT_ARROW){
        m_horizontalAxis += 1.f;
    }
    
    if(keyCode==EventKeyboard::KeyCode::KEY_SPACE){
        m_player->jump();
    }
    
}

void Game::onKeyReleased(EventKeyboard::KeyCode keyCode, cocos2d::Event *event){
    
    if (keyCode == EventKeyboard::KeyCode::KEY_LEFT_ARROW)
    {
        m_horizontalAxis += 1.f;
    }
    if (keyCode == EventKeyboard::KeyCode::KEY_RIGHT_ARROW){
        m_horizontalAxis -= 1.f;
    }
}
*/

// (d) Descomentar. Controles virtuales

void Game::onButtonPressed(Button button) {
    
    if(button == Button::BUTTON_ACTION)
    {
        m_player->jump();
    }
    if(button == Button::BUTTON_LEFT)
    {
        m_horizontalAxis -= 1.f;
    }
    if(button == Button::BUTTON_RIGHT)
    {
        m_horizontalAxis += 1.f;
    }
}

void Game::onButtonReleased(Button button) {
    if(button == Button::BUTTON_LEFT)
    {
        m_horizontalAxis += 1.f;
    }
    if(button == Button::BUTTON_RIGHT)
    {
        m_horizontalAxis -= 1.f;
    }
}

void Game::calcularFinRun() {

    time_t tiempo_actual;
    double seconds_diferencia;
    double tiempo_run;

    tiempo_actual = time (NULL);

    seconds_diferencia = difftime(tiempo_actual, tiempo_inicio_run);

    tiempo_run = 5;

    if (seconds_diferencia >= tiempo_run)
    {
        run = false;
    }
}
