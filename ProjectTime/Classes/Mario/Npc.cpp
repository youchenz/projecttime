//
//  Npc.cpp
//  VideojuegosCocos
//
//  Created by Fidel Aznar on 16/1/15.
//
//

#include "Npc.h"
#include "Mundo.h"


bool NPC::init(){
    GameEntity::init();
    m_sprite = Sprite::create();
    return true;
}

void NPC::preloadResources(){
    
    //Cache de sprites
    auto spriteFrameCache = SpriteFrameCache::getInstance();
    //Cache de animaciones
    auto animCache = AnimationCache::getInstance();
    
    //Si no estaba el spritesheet en la caché lo cargo
    if(!spriteFrameCache->getSpriteFrameByName("skeleton-1.png")) {
        spriteFrameCache->addSpriteFramesWithFile("skeleton-walk.plist");
    }
    
    m_anim = Animation::create();
    
    m_anim->addSpriteFrame(spriteFrameCache->getSpriteFrameByName("skeleton-1.png"));
    m_anim->addSpriteFrame(spriteFrameCache->getSpriteFrameByName("skeleton-2.png"));
    m_anim->addSpriteFrame(spriteFrameCache->getSpriteFrameByName("skeleton-3.png"));
    m_anim->addSpriteFrame(spriteFrameCache->getSpriteFrameByName("skeleton-4.png"));
    m_anim->addSpriteFrame(spriteFrameCache->getSpriteFrameByName("skeleton-5.png"));
    m_anim->addSpriteFrame(spriteFrameCache->getSpriteFrameByName("skeleton-6.png"));
    m_anim->addSpriteFrame(spriteFrameCache->getSpriteFrameByName("skeleton-7.png"));
    m_anim->addSpriteFrame(spriteFrameCache->getSpriteFrameByName("skeleton-8.png"));
    m_anim->addSpriteFrame(spriteFrameCache->getSpriteFrameByName("skeleton-9.png"));
    m_anim->addSpriteFrame(spriteFrameCache->getSpriteFrameByName("skeleton-10.png"));
    m_anim->addSpriteFrame(spriteFrameCache->getSpriteFrameByName("skeleton-11.png"));
    m_anim->addSpriteFrame(spriteFrameCache->getSpriteFrameByName("skeleton-12.png"));
    m_anim->addSpriteFrame(spriteFrameCache->getSpriteFrameByName("skeleton-13.png"));
    m_anim->setDelayPerUnit(.1);
    
    animCache->addAnimation(m_anim, "skeletonAndar");
    
}


Node* NPC::getNode(){
    
    if(m_node==NULL) {
        Size visibleSize = Director::getInstance()->getVisibleSize();
        Vec2 origin = Director::getInstance()->getVisibleOrigin();
        
        m_sprite = Sprite::createWithSpriteFrameName("skeleton-2.png");
        m_sprite->setAnchorPoint(Vec2(0.5,0));
        m_sprite->setPosition(visibleSize.width/2 + origin.x + initPos.x, origin.y + initPos.y);
        m_node = m_sprite;
        m_node->setLocalZOrder(0);
        
        Action *actionAnimate = RepeatForever::create(Animate::create(AnimationCache::getInstance()->getAnimation("skeletonAndar")));
        auto mov1 = MoveBy::create(3, Point(100,0));
        auto flip1 = FlipX::create(true);
        auto mov2 = MoveBy::create(3, Point(-100,0));
        auto flip2 = FlipX::create(false);
        Action* actionMove = RepeatForever::create(Sequence::create(mov1, flip1, mov2, flip2, NULL));
        m_sprite->runAction(actionAnimate);
        m_sprite->runAction(actionMove);
    }
    
    return m_node;
    
}


void NPC::update(float delta){
    
}


